<?php
namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\Event;

class SendSms extends Component {

  public $partner_id;
  public $partner_key;
  public $api_url;
  public $lastMessage;

  private function getTime()
  {
    return round(microtime(true) * 1000);
  }

  private function getHeaders()
  {
    date_default_timezone_set("UTC");
    $time = $this->getTime();
    $hash = sha1(($this->partner_key . $this->getTime()));
    return [
      'Content-type' => 'application/json',
      'Service' => "{$this->partner_id};{$hash};{$time}",
    ];
  }

  public function send($phone, $message, $id = 1)
  {
    $responses = (new \yii\httpclient\Client())->post(
      $this->api_url,
      json_encode([
        'p_id' => $id,
        'number' => (int) $phone,
        'message' => $message
      ]),
      $this->getHeaders()
    )->send();

    $this->lastMessage = $responses->content;
    return strpos($responses->content, 'Запрос успешно обработан') !== FALSE;
  }
}