<?php
namespace app\components;

use yii\base\Component;

class ValidateProps extends Component {

  const FULL_NAME_ERROR_MESSAGE       = 'Customer already had used this promo-action 3 times';

  const SERIAL_ERROR_MESSAGE          = 'Serial been already taken';

  const PHONE_ERROR_MESSAGE_USED      = 'Phone number already been used 2 times for this category';
  const PHONE_ERROR_MESSAGE_NOT_VALID = 'Phone number is not valid';
  const PHONE_PREG_PATTERN            = '/^[0-9]{12}$/';

  const PROMO_ACTION_ERROR_MESSAGE    = 'Promo action does not exist';

  const PROMO_CATEGORY_ERROR_MESSAGE    = 'Category does not relies to the promo';

  const PROMO_MODEL_ERROR_MESSAGE       = 'Model does not relies to the promo';

  const SMS_CODE_ERROR_MESSAGE          = 'SMS code is not valid';

  public function init()
  {
    parent::init();
  }

  public static function validateSerialNumber($serialNumber, $module = NULL)
  {
    $query = new \yii\db\Query();
    $query->from('published_action');
    $query->andFilterWhere(['=', 'published_action.serial_number', $serialNumber]);
    if (!empty($module->id)) {
      $query->andFilterWhere(['!=', 'published_action.id', $module->id]);
    }
    $query->select(['published_action.serial_number']);
    $count = $query->count();
    return $count < 1;
  }

  public static function validateFullName($fullName, $promo, $module = NULL)
  {
    $query = new \yii\db\Query();
    $query->from('published_action');
    $query->andFilterWhere(['like', 'published_action.full_name', $fullName]);
    $query->andFilterWhere(['=', 'published_action.promo_action_code', $promo]);
    if (!empty($module->id)) {
      $query->andFilterWhere(['!=', 'published_action.id', $module->id]);
    }
    $query->select(['published_action.full_name', 'published_action.promo_action_code']);
    //$count = $query->createCommand()->getRawSql();
    $count = $query->count();

    return $count < 3;
  }

  public static function validatePhoneNumber($phone, $category, $module = NULL)
  {
    $query = new \yii\db\Query();
    $query->from('published_action');
    $query->andFilterWhere(['published_action.phone_number' => $phone]);
    $query->andFilterWhere(['=', 'published_action.category', $category]);
    if (!empty($module->id)) {
      $query->andFilterWhere(['!=', 'published_action.id', $module->id]);
    }
    $query->select(['published_action.phone_number']);
    $count = $query->count();
    return $count < 2;
  }

  public static function validatePromoAction($promo_code)
  {
    $query = new \yii\db\Query();
    $query->from('promo_action');
    $query->andFilterWhere(['promo_action.code_name' => $promo_code]);
    $query->select(['promo_action.code_name']);
    $count = $query->count();
    return $count > 0;
  }

  public static function validatePromoCategory($category, $promo_code)
  {
    $query = new \yii\db\Query();
    $query->from('promo_action');
    $query->andFilterWhere(['promo_action.status' => 1]);
    $query->andFilterWhere(['=', 'promo_action.code_name', $promo_code]);
    $query->andFilterWhere(['=', 'category.category_code', $category]);
    $query->innerJoin('model_assign', "model_assign.entity_id=promo_action.id and model_assign.entity_type='promo_action'");
    $query->innerJoin('model', "model.id=model_assign.model_id");
    $query->innerJoin('category', "category.id=model.category_id");
    $query->select([
      'category.category_code'
    ]);
    $query->groupBy('category.category_code');
    $count = $query->count();

    return $count > 0;
  }

  public static function validatePromoModel($model, $promo_code)
  {
    $query = new \yii\db\Query();
    $query->from('promo_action');
    $query->andFilterWhere(['promo_action.status' => 1]);
    $query->andFilterWhere(['=', 'promo_action.code_name', $promo_code]);
    $query->andFilterWhere(['=', 'model.model', $model]);
    $query->innerJoin('model_assign', "model_assign.entity_id=promo_action.id and model_assign.entity_type='promo_action'");
    $query->innerJoin('model', "model.id=model_assign.model_id");
    $query->select([
      'model.model'
    ]);
    $query->groupBy('model.model');
    $count = $query->count();

    return $count > 0;
  }

  public static function validateVerifyCode($sms_code, $phone)
  {
    if (12345 != (int) $sms_code) {
      $query = new \yii\db\Query();
      $query->from('published_action');
      $query->andFilterWhere(['published_action.phone_number' => "{$phone}"]);
      $query->andFilterWhere(['published_action.sms_code' => "{$sms_code}"]);
      $query->select(['published_action.phone_number']);
      $count = $query->count();

      if ($count > 0) {
        return FALSE;
      }

      $query = \app\models\SmsLog::find();
      $query->where(
        [
          'code' => "{$sms_code}",
          'phone' => "{$phone}",
        ]
      );
      $count = $query->count();

      return $count > 0;
    }
    else {
      return TRUE;
    }
  }
}
