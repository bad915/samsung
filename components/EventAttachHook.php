<?php
namespace app\components;

use Yii;
use yii\base\Component;
use yii\base\Event;
use yii\db\ActiveRecord;

class EventAttachHook extends Component {
  public function init()
  {
    parent::init();

    // for any type of entities
    Event::on(ActiveRecord::class, ActiveRecord::EVENT_BEFORE_UPDATE, ['\app\components\EventAttachHook', 'beforeUpdate']);
    Event::on(ActiveRecord::class, ActiveRecord::EVENT_BEFORE_INSERT, ['\app\components\EventAttachHook', 'beforeInsert']);

    // only for published actions
    Event::on(\app\models\PublishedAction::class, \app\models\PublishedAction::EVENT_BEFORE_INSERT, ['\app\components\EventAttachHook', 'beforeInsertPublishedAction']);
  }

  public static function beforeUpdate($event)
  {
    $attributes = $event->sender->attributes();
    if (in_array('updated_at', $attributes) && empty($event->sender->updated_at)) {
      $event->sender->updated_at = time();
    }
  }

  public static function beforeInsert($event)
  {
    $attributes = $event->sender->attributes();
    if (in_array('created_at', $attributes) && empty($event->sender->created_at)) {
      $event->sender->created_at = time();
    }
    if (in_array('updated_at', $attributes) && empty($event->sender->updated_at)) {
      $event->sender->updated_at = time();
    }
    if (in_array('status', $attributes) && empty($event->sender->status)) {
      $event->sender->status = 1;
    }
    if (in_array('user_id', $attributes) && empty($event->sender->user_id)) {
      $event->sender->user_id = \Yii::$app->user->id;
    }

    // this is common and default for now
    if (in_array('entity_type', $attributes) && empty($event->sender->entity_type)) {
      $event->sender->entity_type = 'published_action';
    }
  }

  public static function beforeInsertPublishedAction($event)
  {
    $attributes = $event->sender->attributes();
    if (in_array('promoter_user_id', $attributes) && empty($event->sender->promoter_user_id)) {
      $event->sender->promoter_user_id = \Yii::$app->user->id;
    }
  }
}
