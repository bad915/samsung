<?php

namespace app\components;

use app\models\SourceMessage;
use yii\i18n\MissingTranslationEvent;

class TranslationEventHandler
{
  public static function handleMissingTranslation(MissingTranslationEvent $event)
  {
    try {
      //$event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @";
      $sourceMessage = SourceMessage::find()->where(['message' => $event->message])->one();
      if (empty($sourceMessage)) {
        $sourceMessage = new SourceMessage();
        $sourceMessage->category = 'app';
        $sourceMessage->message = $event->message;
        $sourceMessage->save();
      }
    }
    catch (\Exception $e) {}
  }
}