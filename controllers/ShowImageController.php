<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

class ShowImageController extends Controller
{
  /**
   * {@inheritdoc}
   */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['index'],
        'rules' => [
          [
            'actions' => ['index'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'index' => ['get'],
        ],
      ],
    ];
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex($p = NULL)
  {
    $imagePath = Yii::getAlias('@webroot') . '/' . $p;

    if (file_exists($imagePath)) {
      $filename = $imagePath;
      $handle = fopen($filename, "rb");
      $contents = fread($handle, filesize($filename));
      fclose($handle);
      header("content-type: " . mime_content_type($filename));
      echo $contents;
    } else {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_HTML,
        'statusCode' => 404,
      ]);
    }
  }
}
