<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class PublishedActionImageUploadForm extends Model
{
  /**
   * @var UploadedFile
   */
  public $image;

  public function rules()
  {
    return [
      [['image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, pdf, jpeg'],
    ];
  }

  public function upload($filename)
  {
    if ($this->validate()) {
      $fileFullname = 'uploads/user-images/' . $filename . '.' . $this->image->extension;
      $this->image->saveAs($fileFullname);
      return $fileFullname;
    } else {
      return false;
    }
  }
}