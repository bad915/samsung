<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $entity_type
 * @property int $entity_id
 * @property int $user_id
 * @property string $date
 * @property string $message
 * @property int $created_at
 * @property int $updated_at
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id', 'message'], 'required'],
            [['entity_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            [['message'], 'string'],
            [['entity_type'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_type' => 'Entity Type',
            'entity_id' => 'Entity ID',
            'user_id' => 'User ID',
            'date' => Yii::t('app', 'Date'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
