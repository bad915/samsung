<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\web\IdentityInterface;

class User extends ActiveRecord  implements IdentityInterface
{
  public $password;

  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'user';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['username', 'email', 'password_hash', 'auth_key', 'access_token'], 'required'],
      [['user_status', 'created_at', 'updated_at'], 'integer'],
      [['username'], 'string', 'max' => 25],
      [['email', 'city', 'location'], 'string', 'max' => 255],
      [['password_hash'], 'string', 'max' => 60],
      [['auth_key'], 'string', 'max' => 32],
      [['username'], 'unique'],
      [['email'], 'unique'],
      //[['role'], 'safe'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'username' => 'Username',
      'email' => 'Email',
      'city' => Yii::t('app', 'City'),
      'location' => 'Location',
      'password_hash' => 'Password Hash',
      'auth_key' => 'Auth Key',
      'access_token' => 'Access token',
      'user_status' => 'Status',
      'created_at' => 'Created At',
      'updated_at' => 'Updated At',
    ];
  }

  public static function findIdentity($id)
  {
    return static::findOne($id);
  }

  public static function findIdentityByAccessToken($token, $type = null)
  {
    return static::findOne(['auth_key' => $token, 'user_status' => 1]);
  }

  public static function findByUsername($username)
  {
    return static::findOne(['username' => $username]);
  }

  public function getId()
  {
    return $this->getPrimaryKey();
  }

  public function getAuthKey()
  {
    return $this->auth_key;
  }

  public function validateAuthKey($authKey)
  {
    return $this->getAuthKey() === $authKey;
  }

  public function validatePassword($password)
  {
    return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
  }

  public function setPassword($password)
  {
    $this->password_hash = \Yii::$app->security->generatePasswordHash($password);
  }

  public function setAuthKey()
  {
    $this->auth_key = \Yii::$app->security->generateRandomString();
  }

  public function setAccessToken($password)
  {
    $this->access_token = base64_encode("{$this->username}:{$password}");
  }

  public function getAccessToken()
  {
    return $this->access_token;
  }

  public function setRole($value)
  {
    $auth = Yii::$app->authManager;
    $auth->revokeAll($this->id);

    $promoter = $auth->getRole($value);
    $auth->assign($promoter, $this->id);
  }

  public function getRole()
  {
    $role = (new Query())->from('auth_assignment')->where(['user_id' => $this->id])->select('item_name')->one();
    return $role['item_name'];
  }
}
