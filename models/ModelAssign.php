<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "model_assign".
 *
 * @property string $entity_type
 * @property int $entity_id
 * @property int $model_id
 */
class ModelAssign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'model_assign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_type', 'entity_id', 'model_id'], 'required'],
            [['entity_id', 'model_id'], 'integer'],
            [['entity_type'], 'string', 'max' => 20],
            [['entity_type', 'entity_id', 'model_id'], 'unique', 'targetAttribute' => ['entity_type', 'entity_id', 'model_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'entity_type' => 'Entity Type',
            'entity_id' => 'Entity ID',
            'model_id' => 'Model ID',
        ];
    }
}
