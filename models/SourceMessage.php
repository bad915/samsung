<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "source_message".
 *
 * @property int $id
 * @property string $category
 * @property string $message
 *
 * @property Message[] $messages
 */
class SourceMessage extends \yii\db\ActiveRecord
{

  public $translation;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'source_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'message' => Yii::t('app', 'Message'),
        ];
    }


  public function getTranslation()
  {
    $message = $this->hasOne(Message::className(), ['id' => 'id']);
    return $message->translation;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMessage()
  {
    return $this->hasOne(Message::className(), ['id' => 'id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getMessages()
  {
    return $this->hasMany(Message::className(), ['id' => 'id']);
  }
}
