<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $category_code
 * @property string $category_human_name
 * @property int $created_at
 * @property int $updated_at
 * @property string $image
 */
class Category extends \yii\db\ActiveRecord
{

    public $upload;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_code', 'category_human_name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['category_code'], 'string', 'max' => 10],
            [['category_human_name', 'image'], 'string', 'max' => 255],
            [['category_code'], 'unique'],
            [['upload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_code' => Yii::t('app', 'Category Code'),
            'category_human_name' => Yii::t('app', 'Category Name'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function upload()
    {
      if ($this->validate()) {

        if (!empty($this->image)) {
          if(file_exists(\Yii::getAlias('@webroot') . '/' . $this->image)) {
            unlink(\Yii::getAlias('@webroot') . '/' . $this->image);
          }
        }

        $this->image = 'uploads/' . $this->category_code . '.' . $this->upload->extension;
        $this->upload->saveAs($this->image);
        return true;
      } else {
        return false;
      }
    }
}
