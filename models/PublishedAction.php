<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "published_action".
 *
 * @property int $id
 * @property string $promo_action_code
 * @property string $model
 * @property string $category
 * @property int $promoter_user_id
 * @property string $date
 * @property string $full_name
 * @property string $phone_number
 * @property string $serial_number
 * @property string $image_1
 * @property string $image_2
 * @property string $image_3
 * @property string $sms_code
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 */
class PublishedAction extends \yii\db\ActiveRecord
{

    const REST_CREATE_SCENARIO = 'restCreate';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'published_action';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['promo_action_code', 'model', 'category', 'sms_code', 'full_name', 'phone_number'], 'trim'],
            [[
              'promo_action_code',
              'serial_number',
              'model',
              'category',
              'full_name',
              'phone_number',
              'sms_code',
            ], 'required'],
            [['date'], 'default', 'value' => date('Y-m-d H:i:s')],
            [['promoter_user_id'], 'required', 'when' => function($model) {
              return Yii::$app->controller->module->id != 'api';
            }],
            [['promoter_user_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'safe'],
            [['promo_action_code'], 'string', 'max' => 30],
            [['model'], 'string', 'max' => 50],
            [['category', 'sms_code'], 'string', 'max' => 10],
            [['full_name', 'image_1', 'image_2', 'image_3'], 'string', 'max' => 255],
            [['phone_number'], 'string', 'max' => 12],
            [['serial_number'], 'string', 'max' => 100],
            [['promo_action_code', 'serial_number'], 'unique', 'targetAttribute' => ['promo_action_code', 'serial_number']],
            [['serial_number'], 'unique'],

            // custom system validation
            [['full_name'], \app\validators\FullNameValidator::class, 'on' => self::REST_CREATE_SCENARIO],
            [['serial_number'], \app\validators\SerialNumberValidator::class, 'on' => self::REST_CREATE_SCENARIO],
            [['phone_number'], \app\validators\PhoneNumberValidator::class, 'on' => self::REST_CREATE_SCENARIO],
            [['promo_action_code'], \app\validators\PromoActionValidator::class, 'on' => self::REST_CREATE_SCENARIO],
            [['category'], \app\validators\PromoCategoryValidator::class, 'on' => self::REST_CREATE_SCENARIO],
            [['model'], \app\validators\PromoModelValidator::class, 'on' => self::REST_CREATE_SCENARIO],
            [['sms_code'], \app\validators\SmsCodeValidator::class, 'on' => self::REST_CREATE_SCENARIO],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promo_action_code' => Yii::t('app', 'Promo Action Code'),
            'model' => Yii::t('app', 'Model'),
            'category' => Yii::t('app', 'Category'),
            'promoter_user_id' => Yii::t('app', 'Promoter User ID'),
            'date' => Yii::t('app', 'Date'),
            'full_name' => Yii::t('app', 'Full Name'),
            'phone_number' => Yii::t('app', 'Phone Number'),
            'serial_number' => Yii::t('app', 'Serial Number'),
            'image_1' => Yii::t('app', 'Image 1'),
            'image_2' => Yii::t('app', 'Image 2'),
            'image_3' => Yii::t('app', 'Image 3'),
            'sms_code' => Yii::t('app', 'Sms Code'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getAttributeLabel($attribute)
    {
      return $attribute;
    }
}
