<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_log".
 *
 * @property int $id
 * @property int $user_id
 * @property string $phone
 * @property string $code
 * @property int $created_at
 */
class SmsLog extends \yii\db\ActiveRecord
{

    const UNIVERSAL_VERIFY_CODE = '12345';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['phone', 'code'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['phone'], 'string', 'max' => 12, 'min' => 12],
            [['code'], 'string', 'max' => 20],
            [['phone'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'phone' => Yii::t('app', 'Phone'),
            'code' => Yii::t('app', 'Code'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
