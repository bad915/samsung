<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promo_action".
 *
 * @property int $id
 * @property string $name
 * @property string $code_name
 * @property int $status
 * @property string $start_date
 * @property string $end_date
 * @property string $location
 * @property string $city
 * @property int $created_at
 * @property int $updated_at
 */
class PromoAction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'promo_action';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'code_name', 'status', 'start_date', 'end_date'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['start_date', 'end_date'], 'safe'],
            [['name', 'location', 'city'], 'string', 'max' => 255],
            [['code_name'], 'string', 'max' => 30],
            [['code_name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::t('app', 'Name'),
            'code_name' => Yii::t('app', 'Code Name'),
            'status' => Yii::t('app', 'Status'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'location' => Yii::t('app', 'Location'),
            'city' => Yii::t('app', 'City'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

  public function attributes()
  {
    return array_merge(
      parent::attributes(),
      ['models']
    );
  }

  // only accepts array of model ids
  public function setModels($values)
  {
    \yii\BaseYii::$app->db->createCommand("DELETE FROM `model_assign` WHERE `entity_type` LIKE 'promo_action' AND `entity_id` = {$this->id}")->query();

    foreach ($values as $item) {
      $assign = new \app\models\ModelAssign();
      $assign->entity_type = 'promo_action';
      $assign->entity_id = $this->id;
      $assign->model_id = $item;
      $assign->save();
    }

    return TRUE;
  }

  public function getModels()
  {
    return \app\models\ModelAssign::find()
      ->where(['entity_id' => $this->id, 'entity_type' => 'promo_action'])
      ->innerJoin('model', 'model.id=model_assign.model_id')
      ->select('model.model')
      ->column();
  }
}
