<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PublishedAction;
use yii\data\Pagination;

/**
 * PublishedActionSearch represents the model behind the search form of `app\models\PublishedAction`.
 */
class PublishedActionSearch extends PublishedAction
{

  public $created_at_range;

    /**
     *
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['promo_action_code', 'model', 'category', 'date', 'full_name', 'phone_number', 'serial_number', 'sms_code'], 'safe'],
            [['created_at_range'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param int $promoter_id
     *
     * @return ActiveDataProvider
     */
    public function search($params, $promoter_id = NULL, $pagination = NULL)
    {
        $query = PublishedAction::find();

        if (!empty($params['comment-search'])) {
          $query->innerJoin('comment', 'comment.entity_id=published_action.id');
          $query->andWhere(['like', 'message', $params['comment-search']]);
        }

        // add conditions that should always apply here
        $dataProviderParams = [
          'query' => $query,
        ];
        if (!empty($pagination)) {
          $dataProviderParams['pagination'] = ['pageSize' => $pagination];
        }
        $dataProvider = new ActiveDataProvider($dataProviderParams);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $where = [
          'id' => $this->id,
          'promoter_user_id' => $this->promoter_user_id,
          'date' => $this->date,
          'status' => $this->status,
          'created_at' => $this->created_at,
          'updated_at' => $this->updated_at,
        ];

        if (!empty($promoter_id)) {
          $where['promoter_user_id'] = $promoter_id;
        }

        // grid filtering conditions
        $query->andFilterWhere($where);

      // do we have values? if so, add a filter to our query
      if(!empty($this->created_at_range) && strpos($this->created_at_range, '-') !== false) {
        list($start_date, $end_date) = explode(' - ', $this->created_at_range);
        $start_date = $start_date . ' 00:00:00';
        $end_date = $end_date . ' 23:59:59';
        $query->andFilterWhere(['between', 'created_at', strtotime($start_date), strtotime($end_date)]);
      }

      $query->andFilterWhere(['like', 'promo_action_code', $this->promo_action_code])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'serial_number', $this->serial_number])
            ->andFilterWhere(['like', 'sms_code', $this->sms_code]);
      
      $query->orderBy(['created_at' => SORT_DESC, 'full_name' => SORT_ASC]);

        return $dataProvider;
    }
}
