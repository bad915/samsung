<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "model".
 *
 * @property int $id
 * @property int $category_id
 * @property string $model
 * @property int $created_at
 * @property int $updated_at
 */
class ProductModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'model';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'model'], 'required'],
            [['category_id', 'created_at', 'updated_at'], 'integer'],
            [['model'], 'string', 'max' => 20],
            [['category_id', 'model'], 'unique', 'targetAttribute' => ['category_id', 'model']],
            [['category'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'model' => Yii::t('app', 'Model'),
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function attributes()
    {
      return array_merge(
        parent::attributes()
      );
    }

    public function setCategory($value)
    {
// has to be written
    }

    public function getCategory()
    {
      return Category::find()->where(['id' => $this->category_id])->one()->category_human_name;
    }
}
