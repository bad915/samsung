<?php

namespace app\validators;

use yii\validators\Validator;
use \app\components\ValidateProps;

class FullNameValidator extends Validator // only for PublishedAction
{
  public function validateAttribute($model, $attribute)
  {
    if (!ValidateProps::validateFullName(
      $model->$attribute,
      $model->promo_action_code,
      $model
    )) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::FULL_NAME_ERROR_MESSAGE));
    }
  }
}