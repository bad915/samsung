<?php

namespace app\validators;

use yii\validators\Validator;
use \app\components\ValidateProps;

class SmsCodeValidator extends Validator
{
  public function validateAttribute($model, $attribute)
  {
    if (
      !ValidateProps::validateVerifyCode(
        $model->$attribute,
        $model->phone_number
    )) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::SMS_CODE_ERROR_MESSAGE));
    }
  }
}