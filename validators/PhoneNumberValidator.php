<?php

namespace app\validators;

use yii\validators\Validator;
use \app\components\ValidateProps;

class PhoneNumberValidator extends Validator // only for PublishedAction
{
  public function validateAttribute($model, $attribute)
  {
    if (!preg_match(ValidateProps::PHONE_PREG_PATTERN, $model->$attribute)) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::PHONE_ERROR_MESSAGE_NOT_VALID));
    }
    elseif (!ValidateProps::validatePhoneNumber(
      $model->$attribute,
      $model->category,
      $model
    )) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::PHONE_ERROR_MESSAGE_USED));
    }
  }
}