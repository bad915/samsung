<?php

namespace app\validators;

use yii\validators\Validator;
use \app\components\ValidateProps;

class SerialNumberValidator extends Validator // only for PublishedAction
{
  public function validateAttribute($model, $attribute)
  {
    if (!ValidateProps::validateSerialNumber(
      $model->$attribute,
      $model
    )) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::SERIAL_ERROR_MESSAGE));
    }
  }
}