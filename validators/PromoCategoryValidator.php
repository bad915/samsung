<?php

namespace app\validators;

use yii\validators\Validator;
use \app\components\ValidateProps;

class PromoCategoryValidator extends Validator // only for PublishedAction
{
  public function validateAttribute($model, $attribute)
  {
    if (!ValidateProps::validatePromoCategory(
      $model->$attribute,
      $model->promo_action_code
    )) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::PROMO_CATEGORY_ERROR_MESSAGE));
    }
  }
}