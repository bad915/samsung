<?php

namespace app\validators;

use yii\validators\Validator;
use \app\components\ValidateProps;

class PromoModelValidator extends Validator // only for PublishedAction
{
  public function validateAttribute($model, $attribute)
  {
    if (!ValidateProps::validatePromoModel(
      $model->$attribute,
      $model->promo_action_code
    )) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::PROMO_MODEL_ERROR_MESSAGE));
    }
  }
}