<?php

namespace app\validators;

use yii\validators\Validator;
use \app\components\ValidateProps;

class PromoActionValidator extends Validator // only for PublishedAction
{
  public function validateAttribute($model, $attribute)
  {
    if (!ValidateProps::validatePromoAction(
      $model->$attribute
    )) {
      $this->addError($model, $attribute, \Yii::t('app', ValidateProps::PROMO_ACTION_ERROR_MESSAGE));
    }
  }
}