<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$sms = require __DIR__ . '/sms.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'EventAttachHook'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [
      'admin' => [
        'class' => 'app\modules\admin\AdminModule'
      ],
      'api' => [
        'class' => 'app\modules\api\ApiModule'
      ],
      'moderator' => [
        'class' => 'app\modules\moderator\ModeratorModule',
      ],
    ],
    'components' => [
        'EventAttachHook' => [
          'class' => '\app\components\EventAttachHook'
        ],
        'user' => [
          'identityClass' => 'app\models\User',
          'enableAutoLogin' => true,
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'baseUrl' => '',
            'cookieValidationKey' => 'FFHSow1-QpTgyR1TQFfGmpK0H4K5CiEb',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => 0,
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
              '<alias:\w+>' => 'site/<alias>',
              '<alias:\w+/\w{0}>' => 'site/<alias>'
            ],
        ],
        'assetManager' => [
          'class' => 'yii\web\AssetManager',
          'forceCopy' => true,
          'appendTimestamp' => true,
        ],
        'authManager' => [
          'class' => 'yii\rbac\DbManager',
        ],
        'sms' => $sms,
        'thumbnail' => [
          'class' => 'sadovojav\image\Thumbnail',
          'cachePath' => '@webroot/thumbnails'
        ],
        'i18n' => [
          'translations' => [
            'app' => [
              'sourceLanguage' => 'en-US',
              'class' => 'yii\i18n\DbMessageSource',
              'enableCaching' => FALSE,
              'db' => $db,
              'on missingTranslation' => ['app\components\TranslationEventHandler', 'handleMissingTranslation']
            ],
          ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
