<?php

use yii\db\Migration;

/**
 * Class m181117_083658_add_image_category
 */
class m181117_083658_add_image_category extends Migration
{

    public function up()
    {
      $this->addColumn('category', 'image', $this->string(255)->null());
    }

    public function down()
    {
      $this->dropColumn('category', 'image');
    }

}
