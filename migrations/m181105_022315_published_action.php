<?php

use yii\db\Migration;

/**
 * Class m181105_022315_published_action
 */
class m181105_022315_published_action extends Migration
{
  public function up()
  {
    $this->createTable('published_action', [
      'id' => $this->primaryKey(),
      'promo_action_code' => $this->string(30)->notNull(),
      'model' => $this->string(50)->null(),
      'category' => $this->string(10)->null(),
      'promoter_user_id' => $this->integer()->notNull(),
      'date' => $this->dateTime()->null(),
      'full_name' => $this->string(255)->null(),
      'phone_number' => $this->string(12)->null(),
      'serial_number' => $this->string(100)->notNull(),
      'image_1' => $this->string(255)->null(),
      'image_2' => $this->string(255)->null(),
      'image_3' => $this->string(255)->null(),
      'sms_code' => $this->string(10)->null(),
      'status' => $this->integer(1)->notNull(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ]);

    $this->createIndex('{{%published_action_model}}', '{{%published_action}}', 'model', false);
    $this->createIndex('{{%published_action_category}}', '{{%published_action}}', 'category', false);
    $this->createIndex('{{%published_action_promo_action_code}}', '{{%published_action}}', 'promo_action_code', false);
    $this->createIndex('{{%published_action_full_name}}', '{{%published_action}}', 'full_name', false);
    $this->createIndex('{{%published_action_serial_number}}', '{{%published_action}}', 'serial_number', false);
    $this->createIndex('{{%published_action_promo_serial}}', '{{%published_action}}', 'promo_action_code,serial_number', true);
  }

  /**
   * {@inheritdoc}
   */
  public function Down()
  {
    $this->delete('published_action');
  }
}
