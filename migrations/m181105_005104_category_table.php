<?php

use yii\db\Migration;

/**
 * Class m181105_005104_category_table
 */
class m181105_005104_category_table extends Migration
{
  public function up()
  {
    $this->createTable('category', [
      'id' => $this->primaryKey(),
      'category_code' => $this->string(10)->notNull(),
      'category_human_name' => $this->string(255)->notNull(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ]);
    $this->createIndex('{{%category_category_code}}', '{{%category}}', 'category_code', true);
  }
  /**
   * {@inheritdoc}
   */
  public function Down()
  {
    $this->delete('category');
  }
}
