<?php

use yii\db\Migration;

/**
 * Class m181105_005118_models_table
 */
class m181105_005118_models_table extends Migration
{
  public function up()
  {
    $this->createTable('model', [
      'id' => $this->primaryKey(),
      'category_id' => $this->integer()->notNull(),
      'model' => $this->string(20)->notNull(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ]);
    $this->createIndex('{{%model_code}}', '{{%model}}', 'model', false);
    $this->createIndex('{{%model_category}}', '{{%model}}', 'category_id,model', true);
  }

  /**
   * {@inheritdoc}
   */
  public function Down()
  {
    $this->delete('model');
  }
}
