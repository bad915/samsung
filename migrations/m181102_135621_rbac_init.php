<?php

use yii\db\Migration;

/**
 * Class m181102_135621_rbac_init
 */
class m181102_135621_rbac_init extends Migration
{
  public function up()
  {
    $auth = Yii::$app->authManager;

    $manageArticles = $auth->createPermission('siteUser');
    $manageArticles->description = 'Site user';
    $auth->add($manageArticles);

    $manageUsers = $auth->createPermission('administerSite');
    $manageUsers->description = 'Administer site';
    $auth->add($manageUsers);

    $moderator = $auth->createRole('moderator');
    $moderator->description = 'Moderator';
    $auth->add($moderator);
    $auth->addChild($moderator, $manageArticles);

    $admin = $auth->createRole('admin');
    $admin->description = 'Administrator';
    $auth->add($admin);
    $auth->addChild($admin, $moderator);
    $auth->addChild($admin, $manageUsers);
  }

  public function down()
  {
    Yii::$app->authManager->removeAll();
  }
}
