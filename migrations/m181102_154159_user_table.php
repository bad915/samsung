<?php

use yii\db\Migration;

/**
 * Class m181102_154159_user_table
 */
class m181102_154159_user_table extends Migration
{
  // Use up()/down() to run migration code without a transaction.
  public function up()
  {
    $this->createTable('user', [
      'id' => $this->primaryKey(),
      'username' => $this->string(25)->notNull(),
      'email' => $this->string(255)->notNull(),
      'city' => $this->string(255)->null(),
      'location' => $this->string(255)->null(),
      'password_hash' => $this->string(60)->notNull(),
      'auth_key' => $this->string(32)->notNull(),
      'access_token' => $this->string(32)->notNull(),
      'user_status' => $this->integer()->null(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ]);
    $this->createIndex('{{%user_unique_username}}', '{{%user}}', 'username', true);
    $this->createIndex('{{%user_unique_email}}', '{{%user}}', 'email', true);
  }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181102_154159_user_table cannot be reverted.\n";

        return false;
    }

}
