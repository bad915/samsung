<?php

use yii\db\Migration;

/**
 * Class m181105_021710_model_assign_table
 */
class m181105_021710_model_assign_table extends Migration
{
  public function up()
  {
    $this->createTable('model_assign', [
      'entity_type' => $this->string(20)->notNull(),
      'entity_id' => $this->integer()->notNull(),
      'model_id' => $this->integer()->notNull(),
    ]);
    $this->createIndex('{{%model_assign}}', '{{%model_assign}}', 'entity_type,entity_id,model_id', true);
  }

  /**
   * {@inheritdoc}
   */
  public function Down()
  {
    $this->delete('model_assign');
  }
}
