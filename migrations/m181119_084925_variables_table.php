<?php

use yii\db\Migration;

/**
 * Class m181119_084925_variables_table
 */
class m181119_084925_variables_table extends Migration
{
  public function up()
  {
    $this->createTable('variables', [
      'name' => $this->string(255)->notNull(),
      'value' => $this->text()->null(),
    ]);
    $this->createIndex('{{%variables_name}}', '{{%variables}}', 'name', true);

    $sql = 'ALTER TABLE `variables` CHANGE `value` `value` LONGBLOB NULL DEFAULT NULL; ';
    $sql .= "ALTER TABLE `variables` ADD PRIMARY KEY(`name`);";
    \yii\BaseYii::$app->db->createCommand($sql)->query();
  }

  /**
   * {@inheritdoc}
   */
  public function down()
  {
    $this->delete('variables');
  }

}
