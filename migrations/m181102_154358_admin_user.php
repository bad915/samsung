<?php

use yii\db\Migration;

/**
 * Class m181102_154358_admin_user
 */
class m181102_154358_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
      $user = new app\models\User();
      $password = 'admin';
      $user->setPassword('admin');
      $user->username = 'admin';
      $user->email = 'admin@admin.com';
      $user->status = 1;
      $user->created_at = time();
      $user->updated_at = time();
      $user->setAuthKey();
      $user->setAccessToken($password);
      $user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181102_154358_admin_user cannot be reverted.\n";

        return false;
    }

}
