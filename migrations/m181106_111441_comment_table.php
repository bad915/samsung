<?php

use yii\db\Migration;

/**
 * Class m181106_111441_comment_table
 */
class m181106_111441_comment_table extends Migration
{
  public function up()
  {
    $this->createTable('comment', [
      'id' => $this->primaryKey(),
      'entity_type' => $this->string(15)->null(),
      'entity_id' => $this->integer()->notNull(),
      'user_id' => $this->integer()->notNull(),
      'date' => $this->dateTime()->null(),
      'message' => $this->text()->notNull(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ]);
    $this->createIndex('{{%comment_entity_id}}', '{{%comment}}', 'entity_id', false);
  }

  /**
   * {@inheritdoc}
   */
  public function Down()
  {
    $this->delete('comment');
  }
}
