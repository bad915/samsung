<?php

use yii\db\Migration;

/**
 * Class m181113_084312_phone_sms_log_table
 */
class m181113_084312_phone_sms_log_table extends Migration
{
  public function up()
  {
    $this->createTable('sms_log', [
      'id' => $this->primaryKey(),
      'user_id' => $this->integer()->notNull(),
      'phone' => $this->string(12)->notNull(),
      'code' => $this->string(20)->notNull(),
      'created_at' => $this->integer()->notNull(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function Down()
  {
    $this->delete('sms_log');
  }
}
