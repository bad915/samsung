<?php

use yii\db\Migration;

/**
 * Class m181105_020655_promo_action_table
 */
class m181105_020655_promo_action_table extends Migration
{
  public function up()
  {
    $this->createTable('promo_action', [
      'id' => $this->primaryKey(),
      'name' => $this->string(255)->notNull(),
      'code_name' => $this->string(30)->notNull(),
      'status' => $this->integer(1)->notNull(),
      'start_date' => $this->dateTime()->notNull(),
      'end_date' => $this->dateTime()->notNull(),
      'location' => $this->string(255)->null(),
      'city' => $this->string(255)->null(),
      'created_at' => $this->integer()->notNull(),
      'updated_at' => $this->integer()->notNull(),
    ]);
    $this->createIndex('{{%promo_action_name}}', '{{%promo_action}}', 'name', false);
    $this->createIndex('{{%promo_action_code_name}}', '{{%promo_action}}', 'code_name', true);
  }

  /**
   * {@inheritdoc}
   */
  public function Down()
  {
    $this->delete('promo_action');
  }
}
