<?php

use yii\db\Migration;

/**
 * Class m181115_093229_alter_tables
 */
class m181115_093229_alter_tables extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
      $sql = 'ALTER TABLE `comment` CHANGE `entity_type` `entity_type` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL; ';
      $sql .= 'ALTER TABLE `model_assign` CHANGE `entity_type` `entity_type` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;';
      \yii\BaseYii::$app->db->createCommand($sql)->query();
    }


}
