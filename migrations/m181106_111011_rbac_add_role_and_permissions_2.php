<?php

use yii\db\Migration;

/**
 * Class m181106_111011_rbac_add_role_and_permissions_2
 */
class m181106_111011_rbac_add_role_and_permissions_2 extends Migration
{
  public function up()
  {
    $auth = Yii::$app->authManager;

    $publishPublishedAction = $auth->createPermission('publishPublishedAction');
    $publishPublishedAction->description = 'Permission for creating entity published_action';
    $auth->add($publishPublishedAction);

    $promoter = $auth->createRole('promoter');
    $promoter->description = 'Promoter';
    $auth->add($promoter);
    $auth->addChild($promoter, $publishPublishedAction);

    $publishComment = $auth->createPermission('publishComment');
    $publishComment->description = 'Permission for publishing comment';
    $auth->add($publishComment);
    $auth->addChild($promoter, $publishComment);

    $admin = $auth->getRole('admin');
    $auth->addChild($admin, $publishComment);
  }

  public function down()
  {
    Yii::$app->authManager->removeAll();
  }
}
