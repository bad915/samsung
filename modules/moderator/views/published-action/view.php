<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use branchonline\lightbox\Lightbox;

/* @var $this yii\web\View */
/* @var $model app\models\PublishedAction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Published Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="published-action-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div style="margin-bottom: 20px;">

            <form action="" method="post" style="display: inline-block;">
              <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                     value="<?=Yii::$app->request->csrfToken?>"/>
              <input type="hidden" name="type" value="excel" />
              <button class="btn btn-info" type="submit"><?=\Yii::t('app', 'Export')?>&nbsp;&nbsp;&nbsp;<i style="font-size: 20px" class="fa fa-file-excel-o"></i></button>
            </form>

            <form action="" method="post" style="display: inline-block;">
              <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                     value="<?=Yii::$app->request->csrfToken?>"/>
              <input type="hidden" name="type" value="photo" />
              <button class="btn btn-info" type="submit"><?=\Yii::t('app', 'Export photos')?></button>
            </form>

    </div>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'promo_action_code',
            'model',
            'category',
            'promoter_user_id' => [
                'label' => 'Promoter',
                'value' => function ($model) {
                  return \app\models\User::find()->where(['id' => $model->promoter_user_id])->one()->email;
                }
            ],
            'date',
            'full_name',
            'phone_number',
            'serial_number',
            'photos:html' => [
              'label' => 'PHOTO',
              'format' => 'html',
              'value' => function($model) {
                $output = '';

                $fields = [
                  'image_1',
                  'image_2',
                  'image_3',
                ];

                foreach ($fields as $field) {
                  if (!empty($model->{$field})) {
                    $url = Yii::$app->thumbnail->url(\Yii::getAlias('@webroot') . '/' . $model->{$field}, [
                      'thumbnail' => [
                        'width' => 50,
                        'height' => 50
                      ]
                    ]);

                    $output .= Lightbox::widget([
                      'files' => [
                        [
                          'thumb' => $url,
                          'original' => '/show-image/?p=' . $model->{$field},
                        ],
                      ]
                    ]);
                  }
                }

                return $output;
              }
            ],
            'sms_code',
        ],
    ]) ?>

</div>

<?php if (!empty($comments)): ?>
  <h2><?=\Yii::t('app', 'Comments')?></h2>

  <?php foreach ($comments as $comment): ?>
    <div class="card-columns comments-section">
      <div class="card m-b-30 card-body">
        <p><strong>@<?=$comment['user']?></strong></p>
        <p class="card-text"><?=$comment['message']?></p>
        <p class="card-text">
          <small class="text-muted"><?=$comment['date']?></small>
        </p>
      </div>
    </div>
  <?php endforeach;?>
<?php endif; ?>