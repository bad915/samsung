<?php

use yii\helpers\Html;
use yii\grid\GridView;
use branchonline\lightbox\Lightbox;
use jino5577\daterangepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PublishedActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Published Actions');
?>
<!--
https://phpspreadsheet.readthedocs.io/en/develop/
-->
<h1 style="margin-bottom: 20px;"><?= Html::encode($this->title) ?></h1>

<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-12">
        <form action="" method="post" style="display: inline-block;">
          <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                 value="<?=Yii::$app->request->csrfToken?>"/>
          <input type="hidden" name="type" value="excel" />
          <label class="btn"><strong><?= \Yii::t('app', 'Export')?></strong> : </label>
          <button class="btn btn-info" type="submit"><?=\Yii::t('app', 'Export')?>&nbsp;&nbsp;&nbsp;<i style="font-size: 20px" class="fa fa-file-excel-o"></i></button>
        </form>
        
        <form action="" method="post" style="display: inline-block;">
          <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                 value="<?=Yii::$app->request->csrfToken?>"/>
          <input type="hidden" name="type" value="photo" />
          <button class="btn btn-info" type="submit"><?=\Yii::t('app', 'Export photos')?></button>
        </form>
    </div>
</div>

<?php if (!empty($promo_actions)): ?>
<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-12">
        <label class="btn"><strong><?= \Yii::t('app', 'Active promos')?></strong> : </label>
        <?php foreach ($promo_actions as $action): ?>
        <a class="btn btn-primary" href="/moderator/published-action/?PublishedActionSearch[promo_action_code]=<?=$action['code_name']?>">
            <?=$action['code_name']?>
        </a>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>

<div class="published-action-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'created_at' =>[
                'label' => 'DATE',
                'attribute' => 'created_at',
                'content' => function($model) {
                  return date('d.m.Y', $model->created_at);
                },
              'filter' => DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'created_at_range',
                'pluginOptions' => [
                  'format' => 'd-m-Y',
                  'autoUpdateInput' => false
                ]
              ])
            ],
            'category:text:SHORT',
            'model:text:MODEL',
            'serial_number:text:SN_PROMO',
            'photos' => [
                'headerOptions' => ['style' => 'width:200px;'],
                'label' => 'PHOTO',
                'content' => function($model) {
                  $output = '<nobr>';

                  $fields = [
                      'image_1',
                      'image_2',
                      'image_3',
                  ];

                  foreach ($fields as $field) {
                    if (!empty($model->{$field})) {
                      $url = Yii::$app->thumbnail->url(\Yii::getAlias('@webroot') . '/' . $model->{$field}, [
                        'thumbnail' => [
                          'width' => 50,
                          'height' => 50
                        ]
                      ]);

                      $path = $model->{$field};
                      $output .= '&nbsp;&nbsp;' . Lightbox::widget([
                        'files' => [
                          [
                            'thumb' => $url,
                            'original' => '/show-image/?p=' . $path,
                            'title' => "<a target='_blank' href='/show-image/?p={$path}'>Download</a>"
                          ],
                        ]
                      ]);
                    }
                  }

                  $output .= '</nobr>';

                  return $output;
                }
            ],
            'full_name:text:FIO',
            'phone_number:text:TEL',
            'promo_action_code:text:PROMO',
            'comment' => [
                'label' => 'Comments',
                'content' => function ($model) {
                  $output = '<ul>';

                  $comments = \app\models\Comment::find()->where([
                    'entity_id' => $model->id,
                    'entity_type' => 'published_action'
                  ])
                    ->asArray()
                    ->all();

                  foreach ($comments as $comment) {
                    $username = \app\models\User::find()->where(['id' => $comment['user_id']])->one()->username;
                    $output .= "
                      <li>
                        <label>{$username}: </label>
                        <div>{$comment['message']}</div>
                      </li>
                    ";
                  }

                  $output .= '</ul>';

                  return $output;
                },
                'filter' => '<input class="form-control" type="text" name="comment-search" value="' . (!empty($_GET['comment-search']) ? $_GET['comment-search'] : '') . '">'
            ],
            [
                'class' => '\app\modules\admin\models\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
</div>
