<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <meta content="Admin Dashboard" name="description" />
  <meta content="ThemeDesign" name="author" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody();?>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader"><div id="status"><div class="spinner"></div></div></div>

<!-- Begin page -->
<div id="wrapper">

  <!-- ========== Left Sidebar Start ========== -->
  <div class="left side-menu">
    <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
      <i class="ion-close"></i>
    </button>

    <!-- LOGO -->
    <div class="topbar-left">
      <div class="text-center">

<!--        <a href="index.html" class="logo"><img src="assets/images/logo.png" height="24" alt="logo"></a>-->
      </div>
    </div>

    <div class="sidebar-inner slimscrollleft">

<!--      <div class="user-details">-->
<!--        <div class="text-center">-->
<!--          <img src="assets/images/users/avatar-6.jpg" alt="" class="rounded-circle">-->
<!--        </div>-->
<!--        <div class="user-info">-->
<!--          <h4 class="font-16">Steven Meyers</h4>-->
<!--          <span class="text-muted user-status"><i class="fa fa-dot-circle-o text-success"></i> Online</span>-->
<!--        </div>-->
<!--      </div>-->

      <div id="sidebar-menu">
        <ul>
<!--          <li class="menu-title">Main</li>-->
<!---->
          <li>
            <a href="/moderator/default/index" class="waves-effect">
              <i class="ion-social-buffer"></i>
              <span> <?=Yii::t('app', 'Dashboard')?></span>
            </a>
          </li>

          <li>
            <a href="/moderator/promo-action" class="waves-effect">
              <i class="ion-social-buffer"></i>
              <span> <?=Yii::t('app', 'Promo Actions')?></span>
            </a>
          </li>

          <li>
            <a href="/moderator/published-action" class="waves-effect">
              <i class="ion-social-buffer"></i>
              <span> <?=Yii::t('app', 'Published Actions')?></span>
            </a>
          </li>

        </ul>
      </div>
      <div class="clearfix"></div>
    </div> <!-- end sidebarinner -->
  </div>
  <!-- Left Sidebar End -->

  <!-- Start right Content here -->

  <div class="content-page">
    <!-- Start content -->
    <div class="content">

      <!-- Top Bar Start -->
      <div class="topbar">
        <nav class="navbar-custom">

          <?php
          echo Nav::widget([
            'encodeLabels' => false,
            'items' => [['label' => 'Logout', 'url' => ['/site/logout']]],
            'options' => ['class' => 'notification-list float-right'],
          ]);
          ?>

          <ul class="list-inline menu-left mb-0">
            <li class="list-inline-item">
              <button type="button" class="button-menu-mobile open-left waves-effect">
                <i class="ion-navicon"></i>
              </button>
            </li>
            <li class="hide-phone list-inline-item app-search">
              <h3 class="page-title"><?= Html::encode($this->title) ?></h3>
            </li>
          </ul>

          <div class="clearfix"></div>

        </nav>

      </div>
      <!-- Top Bar End -->

      <div class="page-content-wrapper ">

        <div class="container-fluid">
          <?= Alert::widget() ?>

          <?= $content ?>

        </div><!-- container -->


      </div> <!-- Page content Wrapper -->

    </div> <!-- content -->

    <footer class="footer">
      © something
    </footer>

  </div>
  <!-- End Right content here -->

</div>
<!-- END wrapper -->


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
