<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PromoAction */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Promo Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="promo-action-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'code_name',
            'status',
            'start_date',
            'end_date',
            'created_at:datetime',
        ],
    ]) ?>

</div>
