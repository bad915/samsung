<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PromoActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promo Actions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-action-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'code_name',
            'status',
            'start_date',
            'end_date',

            [
                'class' => '\app\modules\admin\models\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]); ?>
</div>
