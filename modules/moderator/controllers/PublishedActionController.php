<?php

namespace app\modules\moderator\controllers;

use Yii;
use app\models\PublishedAction;
use app\models\PublishedActionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\controllers\PublishedActionController as BasePublishedActionController;
use app\models\Variables;

/**
 * PublishedActionController implements the CRUD actions for PublishedAction model.
 */
class PublishedActionController extends Controller
{

  /**
   * Lists all PublishedAction models.
   * @return mixed
   */
  public function actionIndex()
  {
    $searchModel = new PublishedActionSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, 25);

    $permitted_promos = (array) Variables::get('permitted_promos_' . \Yii::$app->user->id);
    if (!empty($permitted_promos)) {
      $dataProvider->query->andWhere(['in', 'promo_action_code', $permitted_promos]);
    }

    $permitted_categories = (array) Variables::get('permitted_categories_' . \Yii::$app->user->id);
    if (!empty($permitted_categories)) {
      $dataProvider->query->andWhere(['in', 'category', $permitted_categories]);
    }

    $post = \Yii::$app->request->post();

    $query = \app\models\PromoAction::find()
      ->select(['name', 'code_name']);
    $query->andFilterWhere(['<', 'start_date', date('Y-m-d H:i:t')]);
    $query->andFilterWhere(['>', 'end_date', date('Y-m-d H:i:t')]);

    $permitted_promos = (array) Variables::get('permitted_promos_' . \Yii::$app->user->id);
    if (!empty($permitted_promos)) {
      $query->andFilterWhere(['in', 'code_name', $permitted_promos]);
    }

    $promoActions = $query->asArray()->all();

    if (!empty($post) && $post['type'] == 'excel') {
      BasePublishedActionController::exportXml($dataProvider->query);
    }

    if (!empty($post) && $post['type'] == 'photo') {
      BasePublishedActionController::exportPhotos($dataProvider->query);
    }

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
      'promo_actions' => $promoActions
    ]);
  }

  /**
   * Displays a single PublishedAction model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    $post = \Yii::$app->request->post();

    if (!empty($post) && $post['type'] == 'photo') {
      BasePublishedActionController::exportPhotos(PublishedAction::find()->where(['id' => $id]));
    }

    if (!empty($post) && $post['type'] == 'excel') {
      BasePublishedActionController::exportXml(PublishedAction::find()->where(['id' => $id]));
    }

    $comments = \app\models\Comment::find()->where([
      'entity_id' => $id,
      'entity_type' => 'published_action'
    ])
      ->asArray()
      ->all();

    foreach ($comments as &$comment) {
      $user = \app\models\User::find()->where(['id' => $comment['user_id']])->one();
      if (!empty($user)) {
        $comment['user'] = $user->username;
      }
      else {
        $comment['user'] = $comment['user_id'];
      }
    }

    return $this->render('view', [
      'model' => $this->findModel($id),
      'comments' => $comments
    ]);
  }

  /**
   * Finds the PublishedAction model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return PublishedAction the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id)
  {
    if (($model = PublishedAction::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
