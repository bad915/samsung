<?php

namespace app\modules\moderator\controllers;

use app\models\ProductModel;
use app\models\Variables;
use Yii;
use app\models\PromoAction;
use app\models\PromoActionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PromoActionController implements the CRUD actions for PromoAction model.
 */
class PromoActionController extends Controller
{

    /**
     * Lists all PromoAction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromoActionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $permitted_promos = (array) Variables::get('permitted_promos_' . \Yii::$app->user->id);
        if (!empty($permitted_promos)) {
          $dataProvider->query->andWhere(['in', 'code_name', $permitted_promos]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

  /**
   * Displays a single PromoAction model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    return $this->render('view', [
      'model' => $this->findModel($id),
    ]);
  }

    /**
     * Finds the PromoAction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PromoAction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
      $permitted_promos = (array) Variables::get('permitted_promos_' . \Yii::$app->user->id);
      if (empty($permitted_promos)) {
        $model = PromoAction::findOne($id);
      }
      else {
        $model = PromoAction::find()->where(['id' => $id])->andWhere(['in', 'code_name', $permitted_promos])->one();
      }

      if (!empty($model)) {
          return $model;
      }

      throw new NotFoundHttpException('The requested page does not exist.');
    }
}
