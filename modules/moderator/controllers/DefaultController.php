<?php

namespace app\modules\moderator\controllers;

use yii\web\Controller;

/**
 * Default controller for the `Moderator` module
 */
class DefaultController extends Controller
{
  public function actionIndex()
  {
    return $this->render('index');
  }
}
