<?php

namespace app\modules\moderator;

use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\modules\admin\assets\AdminThemeAsset;

/**
 * Moderator module definition class
 */
class ModeratorModule extends \yii\base\Module
{
  /**
   * {@inheritdoc}
   */
  public $controllerNamespace = 'app\modules\moderator\controllers';

  public function init()
  {
    parent::init();

    $this->layout = 'main';
    \Yii::$app->view->title = \Yii::t('app', 'Moderating');
    AdminThemeAsset::register(\Yii::$app->view);
  }

  public function getViewPath()
  {
    return \Yii::getAlias('@app/modules/moderator/views');
  }

  public function getLayoutPath()
  {
    return \Yii::getAlias('@app/modules/moderator/views/layouts');
  }

  /*
* Controller behaviours
*/
  public function behaviors() {
    $behaviors = parent::behaviors();

    $behaviors['access'] = [
      'class' => AccessControl::class,
      'ruleConfig' => [
        'class' => AccessRule::class,
      ],
      'rules' => [
        [
          'allow' => true,
          'roles' => ['moderator'],
        ],
      ],
    ];

    return $behaviors;
  }
}
