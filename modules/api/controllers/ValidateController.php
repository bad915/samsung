<?php
namespace app\modules\api\controllers;

use yii\db\Query;
use \app\components\ValidateProps;

class ValidateController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'index'  => ['get']
        ],
      ],
    ];
  }

  private function inValidResponse($message)
  {
    return \Yii::createObject([
      'class' => 'yii\web\Response',
      'format' => \yii\web\Response::FORMAT_JSON,
      'statusCode' => 400,
      'data' => [
        'message' => $message,
      ],
    ]);
  }

  private function validResponse($message = 'ok')
  {
    return \Yii::createObject([
      'class' => 'yii\web\Response',
      'format' => \yii\web\Response::FORMAT_JSON,
      'statusCode' => 200,
      'data' => [
        'message' => $message,
      ],
    ]);
  }

  public function actionSerial($serial_number)
  {
    if (!ValidateProps::validateSerialNumber($serial_number)) {
      return $this->inValidResponse(\Yii::t('app',ValidateProps::SERIAL_ERROR_MESSAGE));
    }
    else {
      return $this->validResponse();
    }
  }

  public function actionName($full_name, $promo) // client full name
  {
    if (!ValidateProps::validateFullName($full_name, $promo)) {
      return $this->inValidResponse(\Yii::t('app',ValidateProps::FULL_NAME_ERROR_MESSAGE));
    }
    else {
      return $this->validResponse();
    }
  }

  public function actionPhone($phone, $category)
  {
    if (!preg_match(ValidateProps::PHONE_PREG_PATTERN, $phone)) {
      return $this->inValidResponse(\Yii::t('app',ValidateProps::PHONE_ERROR_MESSAGE_NOT_VALID));
    }
    elseif (!ValidateProps::validatePhoneNumber($phone, $category)) {
      return $this->inValidResponse(\Yii::t('app',ValidateProps::PHONE_ERROR_MESSAGE_USED));
    }
    else {
      return $this->validResponse();
    }
  }
}