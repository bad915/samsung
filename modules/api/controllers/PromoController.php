<?php
namespace app\modules\api\controllers;

use yii\db\Query;

class PromoController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'index'  => ['get']
        ],
      ],
    ];
  }

  public function actionIndex()
  {
    $query = new Query();
    $query->from('promo_action');
    $query->andFilterWhere(['promo_action.status' => 1]);
    $query->andFilterWhere(['<', 'promo_action.start_date', date('Y-m-d H:i:t')]);
    $query->andFilterWhere(['>', 'promo_action.end_date', date('Y-m-d H:i:t')]);
    $query->innerJoin('model_assign', "model_assign.entity_id=promo_action.id and model_assign.entity_type='promo_action'");
    $query->innerJoin('model', "model.id=model_assign.model_id");
    $query->innerJoin('category', "category.id=model.category_id");
    $query->select([
      'promo_action.name',
      'promo_action.code_name as promo',
      'promo_action.start_date',
      'promo_action.end_date',
    ]);
    $query->groupBy('promo_action.id');
    $categories = $query->all();

    return \Yii::createObject([
      'class' => 'yii\web\Response',
      'format' => \yii\web\Response::FORMAT_JSON,
      'data' => $categories
    ]);
  }
}