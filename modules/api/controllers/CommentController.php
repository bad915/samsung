<?php
namespace app\modules\api\controllers;

use app\models\PublishedAction;
use yii\db\Query;

class CommentController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'post'  => ['post'],
          'get' => ['get']
        ],
      ],
    ];
  }

  public function actionPost()
  {
    $publish_id = preg_replace('/[^0-9]{0,}/', '', \Yii::$app->request->post('publish_id'));

    if (empty($publish_id)) {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        //'statusCode' => 400, it is not my intention
        'data' => [
          'message' => 'Publish id is wrong or you are not the author'
        ],
      ]);
    }

    $publishModel = PublishedAction::find()->where([
      'id' => $publish_id,
      'promoter_user_id' => \Yii::$app->user->id
    ])->one();

    if (!empty($publishModel) && \Yii::$app->request->post('message') != '') {
      $message = trim(\Yii::$app->request->post('message'));
      $comment = new \app\models\Comment();
      $comment->entity_type = 'published_action';
      $comment->entity_id = $publish_id;
      $comment->date = date('Y-m-d H:i:s');
      $comment->message = filter_var($message, FILTER_SANITIZE_STRING);

      $comment->validate();
      $errors = $comment->getErrors();
      if (!empty($errors)) {
        print_r($errors);
      }

      $comment->save();

      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        'statusCode' => 200,
        'data' => [
          'message' => 'Comment published',
          'object' => $comment->toArray()
        ],
      ]);
    }
    elseif (\Yii::$app->request->post('message') != '') {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        //'statusCode' => 400, it is not my intention
        'data' => [
          'message' => 'Empty message'
        ],
      ]);
    }
    else {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        //'statusCode' => 400, it is not my intention
        'data' => [
          'message' => 'Publish id is wrong or you are not the author'
        ],
      ]);
    }
  }

  public function actionGet($publish_id)
  {
    $publishModel = PublishedAction::find()->where([
      'id' => $publish_id,
      'promoter_user_id' => \Yii::$app->user->id
    ])->one();

    if (!empty($publishModel)) {
      $comments = \app\models\Comment::find()->where([
        'entity_id' => $publish_id,
        'entity_type' => 'published_action',
        'user_id' => \Yii::$app->user->id
      ])
        ->asArray()
        ->all();

      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        'statusCode' => 200,
        'data' => $comments,
      ]);
    }
    else {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        //'statusCode' => 400, it is not my intention
        'data' => [
          'message' => 'Publish id is wrong or you are not the author'
        ],
      ]);
    }
  }
}