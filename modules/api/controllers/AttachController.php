<?php
namespace app\modules\api\controllers;

use app\models\PublishedAction;
use yii\db\Query;

class AttachController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'index'  => ['post']
        ],
      ],
    ];
  }

  public function actionIndex()
  {
    $publish_id = preg_replace('/[^0-9]{0,}/', '', \Yii::$app->request->post('publish_id'));

    if (empty($publish_id)) {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        //'statusCode' => 400, it is not my intention
        'data' => [
          'message' => 'Publish id is wrong or you are not the author'
        ],
      ]);
    }

    $publishModel = PublishedAction::find()->where([
      'id' => $publish_id,
      'promoter_user_id' => \Yii::$app->user->id
    ])->one();
    $file_fields = ['image_1', 'image_2', 'image_3'];

    if (!empty($publishModel)) {
      $noFiles = TRUE;
      $errorMessages  = [];
      $models = [];
      foreach ($file_fields as $field) {
        $file = \yii\web\UploadedFile::getInstanceByName($field);
        if (!empty($file)) {
          $noFiles = FALSE;
          $models[$field] = new \app\models\PublishedActionImageUploadForm();
          $models[$field]->image = \yii\web\UploadedFile::getInstanceByName($field);
          if (!$models[$field]->validate()) {
            $errorMessages[] = [
              $field . ':' . $models[$field]->getErrors()['image'][0]
            ];
          }
        }
      }

      if (!empty($errorMessages) || $noFiles) {
        return \Yii::createObject([
          'class' => 'yii\web\Response',
          'format' => \yii\web\Response::FORMAT_JSON,
          //'statusCode' => 400, it is not my intention
          'data' => [
            'message' => !empty($errorMessages) ? implode(', ', $errorMessages) : 'Empty request, no files'
          ],
        ]);
      }

      foreach ($models as $field => $model) {
        $filename = $publish_id . '_' . $field . '_' . date('Y_m_d');
        $publishModel->{$field} = $model->upload($filename);
      }

      $publishModel->save();

      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        'statusCode' => 200,
        'data' => [
          'message' => 'Images ' . implode(', ', array_keys($models)) . ' were attached successfully',
          'object' => $publishModel->toArray()
        ],
      ]);
    }
    else {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        //'statusCode' => 400, it is not my intention
        'data' => [
          'message' => 'Publish id is wrong or you are not the author'
        ],
      ]);
    }
  }

  public function actionShow($p)
  {
    $imagePath = \Yii::getAlias('@webroot') . '/' . $p;

    if (file_exists($imagePath)) {
      $filename = $imagePath;
      $handle = fopen($filename, "rb");
      $contents = fread($handle, filesize($filename));
      fclose($handle);
      header("content-type: " . mime_content_type($filename));
      echo $contents;
      exit;
    } else {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_HTML,
        'statusCode' => 404,
      ]);
    }
  }
}