<?php
namespace app\modules\api\controllers;

use yii\db\Query;

class CategoryController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'index'  => ['get']
        ],
      ],
    ];
  }

  public function actionIndex($promo = NULL)
  {
    $promo = trim($promo);

    $query = new Query();
    $query->from('promo_action');
    $query->andFilterWhere(['promo_action.status' => 1]);

    if (!empty($promo)) {
      $query->andFilterWhere(['like', 'promo_action.code_name', $promo]);
    }

    $query->innerJoin('model_assign', "model_assign.entity_id=promo_action.id and model_assign.entity_type='promo_action'");
    $query->innerJoin('model', "model.id=model_assign.model_id");
    $query->innerJoin('category', "category.id=model.category_id");
    $query->select([
      'category.category_code',
      'category.category_human_name',
      'category.image',
    ]);
    $query->groupBy('category.category_code');
    $categories = $query->all();

    return \Yii::createObject([
      'class' => 'yii\web\Response',
      'format' => \yii\web\Response::FORMAT_JSON,
      'data' => $categories
    ]);
  }
}