<?php
namespace app\modules\api\controllers;

use Yii;

class SmsController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'index'  => ['post']
        ],
      ],
    ];
  }

  public function getVerificationCode()
  {
    $length = 5;
    $chars = array_merge(range(0,9));
    shuffle($chars);
    $password = implode(array_slice($chars, 0,$length));
    return $password;
  }

  public function actionVerify($phone)
  {
    $log = \app\models\SmsLog::find()->where(['phone' => $phone])->one();

    if (empty($log)) {
      $log = new \app\models\SmsLog();
    }

    $log->phone = $phone;
    $log->code = (string) $this->getVerificationCode();
    $log->validate();
    $errors = $log->getErrors();

    if (!empty($errors)) {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        'statusCode' => 400,
        'data' => [
          'message' => array_shift($errors['phone']),
        ],
      ]);
    }

    $log->save();

    if (Yii::$app->sms->send($phone, "{$log->code}", $log->id)) {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        'statusCode' => 200,
        'data' => [
          'message' => Yii::t('app', 'Verification sms has been sent'),
        ],
      ]);
    }
    else {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        'statusCode' => 400,
        'data' => [
          'message' => Yii::$app->sms->lastMessage,
        ],
      ]);
    }
  }
}