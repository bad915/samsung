<?php

namespace app\modules\api\controllers;

use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;

/**
 * Default controller for the `api` module
 */
class DefaultController extends ActiveController
{
  public $modelClass = 'app\models\Category';

  /**
   * {@inheritdoc}
   */
  public function actions()
  {
    return [
      'index' => [
        'class' => 'yii\rest\IndexAction',
        'modelClass' => $this->modelClass,
        'checkAccess' => [$this, 'checkAccess'],
      ],
      'view' => [
        'class' => 'yii\rest\ViewAction',
        'modelClass' => $this->modelClass,
        'checkAccess' => [$this, 'checkAccess'],
      ],
      'create' => [
        'class' => 'yii\rest\CreateAction',
        'modelClass' => $this->modelClass,
        'checkAccess' => [$this, 'checkAccess'],
        'scenario' => $this->createScenario,
      ],
      'update' => [
        'class' => 'yii\rest\UpdateAction',
        'modelClass' => $this->modelClass,
        'checkAccess' => [$this, 'checkAccess'],
        'scenario' => $this->updateScenario,
      ],
      'delete' => [
        'class' => 'yii\rest\DeleteAction',
        'modelClass' => $this->modelClass,
        'checkAccess' => [$this, 'checkAccess'],
      ],
      'options' => [
        'class' => 'yii\rest\OptionsAction',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function verbs()
  {
    return [
      'index' => ['GET', 'HEAD'],
      'view' => ['GET', 'HEAD'],
      'create' => ['POST'],
      'update' => ['PUT', 'PATCH'],
      'delete' => ['DELETE'],
    ];
  }

  /**
   * Checks the privilege of the current user.
   *
   * This method should be overridden to check whether the current user has the privilege
   * to run the specified action against the specified data model.
   * If the user does not have access, a [[ForbiddenHttpException]] should be thrown.
   *
   * @param string $action the ID of the action to be executed
   * @param object $model the model to be accessed. If null, it means no specific model is being accessed.
   * @param array $params additional parameters
   * @throws ForbiddenHttpException if the user does not have access
   */
  public function checkAccess($action, $model = null, $params = [])
  {
  }
}
