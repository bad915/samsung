<?php
namespace app\modules\api\controllers;

use Yii;

class AuthController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'login'  => ['post']
        ],
      ],
    ];
  }

  public function actionLogin()
  {
    $username = !empty($_POST['username'])?$_POST['username']:'';
    $password = !empty($_POST['password'])?$_POST['password']:'';

    if (empty($username) || empty($password)) {
      return \Yii::createObject([
        'class' => 'yii\web\Response',
        'format' => \yii\web\Response::FORMAT_JSON,
        'statusCode' => 401,
        'data' => [
          'message' => 'Authentication failed',
        ],
      ]);
    }
    else {

      $user = \app\models\User::findByUsername($username);

      if(!empty($user)){
        if($user->validatePassword($password)){
          return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'data' => [
              'message' => 'Authentication success',
              'id' => $user->id,
              'username' => $user->username,
              'token' => $user->auth_key,
            ],
          ]);
        }
        else {
          return \Yii::createObject([
            'class' => 'yii\web\Response',
            'format' => \yii\web\Response::FORMAT_JSON,
            'statusCode' => 401,
            'data' => [
              'message' => 'Authentication failed',
            ],
          ]);
        }
      }
      else {
        return \Yii::createObject([
          'class' => 'yii\web\Response',
          'format' => \yii\web\Response::FORMAT_JSON,
          'statusCode' => 401,
          'data' => [
            'message' => 'Authentication failed'
          ],
        ]);
      }
    }

  }
}