<?php
namespace app\modules\api\controllers;

use yii\db\Query;

class ModelController extends \yii\rest\Controller
{

  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
          'index'  => ['get']
        ],
      ],
    ];
  }

  public function actionIndex($promo = NULL, $category_code = NULL)
  {
    $promo = trim($promo);
    $category_code = trim($category_code);

    $query = new Query();
    $query->from('promo_action');
    $query->andFilterWhere(['promo_action.status' => 1]);

    if (!empty($promo)) {
      $query->andFilterWhere(['like', 'promo_action.code_name', $promo]);
    }

    $query->innerJoin('model_assign', "model_assign.entity_id=promo_action.id and model_assign.entity_type='promo_action'");
    $query->innerJoin('model', "model.id=model_assign.model_id");
    $query->innerJoin('category', "category.id=model.category_id");

    if (!empty($category_code)) {
      $query->andFilterWhere(['like', 'category.category_code', $category_code]);
    }

    $query->select([
      'model.model',
      'category.category_code',
      'category.category_human_name',
    ]);
    $query->groupBy('model.model');
    $categories = $query->all();

    return \Yii::createObject([
      'class' => 'yii\web\Response',
      'format' => \yii\web\Response::FORMAT_JSON,
      'data' => $categories
    ]);
  }
}