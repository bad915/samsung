/*
 Template Name: Upcube - Bootstrap 4 Admin Dashboard
 Author: Themesdesign
 Website: www.themesdesign.in
 File: Float Js
 */


!function($) {
    "use strict";

    var FlotChart = function() {
        this.$body = $("body")
        this.$realData = []
    };

    //creates Pie Chart
    FlotChart.prototype.createPieGraph = function(selector, labels, datas, colors) {
        function legendFormatter(label, series) {
    return '<div ' + 
           'style="font-size:8pt;text-align:center;padding:2px;">' +
           label + ' ' + Math.round(series.percent)+'%</div>';
};
        
        var data = [{
            label: labels[0],
            data: datas[0]
        }, {
            label: labels[1],
            data: datas[1]
        }, {
            label: labels[2],
            data: datas[2]
        }];
        var options = {
            series: {
                pie: {
                    show: true
                }
            },
            legend : {
                show : true,
                labelFormatter: legendFormatter
            },
            grid : {
                hoverable : true,
                clickable : true
            },
            colors : colors,
            tooltip : true,
            tooltipOpts : {
                    content : "%s, %p.0%"
            }
        };
        
        $.plot($(selector), data, options);
    },
        //initializing various charts and components
        FlotChart.prototype.init = function() {

          //Pie graph data
          var pielabels = window.pieChartLabels;
          var datas = window.pieChartDatas;
          //var colors = ['#ffbb44', '#BA4A00', "#2874A6", '#1ABC9C'];
          this.createPieGraph("#pie-chart #pie-chart-container", pielabels , datas);
        },

    //init flotchart
    $.FlotChart = new FlotChart, $.FlotChart.Constructor = FlotChart

}(window.jQuery),

//initializing flotchart
function($) {
    "use strict";
    $.FlotChart.init()
}(window.jQuery);



