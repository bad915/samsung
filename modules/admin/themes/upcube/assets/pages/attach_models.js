(function($) {

  'use strict';

  var table = $('#table').DataTable( {
    data: window.databaleData,
    'deferRender': true,
    'initComplete': function(settings){
      var api = this.api();

      api.cells(
        api.rows(function(idx, data, node){
          return (data.id in window.alreadyAssigned);
        }).indexes(),
        0
      ).checkboxes.select();
    },
    columnDefs: [
      {
        targets: 0,
        data: 'id',
        orderable: 0,
        defaultContent: '',
        'checkboxes': true
      },
      {
        data: 'model',
        targets: 1,
        defaultContent: '',
        orderable: 1,
        className: ''
      },
      {
        data: 'category',
        targets: 2,
        defaultContent: '',
        orderable: 1,
        className: ''
      },
    ],
    'select': {
      'style': 'multi'
    },
    'order': [[1, 'asc']]
  } );

  // Handle form submission event
  $('#attach-models').on('submit', function(e){
    var form = this;

    var rows_selected = table.column(0).checkboxes.selected();

    $('.hidden-inputs').remove();

    // Iterate over all selected checkboxes
    $.each(rows_selected, function(index, rowId){

      // Create a hidden element
      $(form).append(
        $('<input>')
          .attr('class', 'hidden-inputs')
          .attr('type', 'hidden')
          .attr('name', 'models[]')
          .val(rowId)
      );
    });
  });

}).apply(this, [jQuery]);