<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminThemeAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/themes/upcube';
    public $basePath = '@app/modules/admin/themes/upcube';
    public $css = [
      'assets/css/icons.css',
      'assets/css/style.css',
      'assets/css/custom.css'
    ];

    public $js = [
      'assets/js/jquery.min.js',
      'assets/js/popper.min.js',
      'assets/js/bootstrap.min.js',
      'assets/js/modernizr.min.js',
      'assets/js/detect.js',
      'assets/js/fastclick.js',
      'assets/js/jquery.slimscroll.js',
      'assets/js/jquery.blockUI.js',
      'assets/js/waves.js',
      'assets/js/jquery.nicescroll.js',
      'assets/js/jquery.scrollTo.min.js',
      'assets/plugins/morris/morris.min.js',
      'assets/plugins/raphael/raphael-min.js',
      'assets/js/app.js',
    ];

  public $publishOptions = [
    'only' => [
      'assets/fonts/*',
      'assets/css/*',
      'assets/js/*',
      'assets/pages/*',
      'assets/plugins/morris/*',
      'assets/plugins/raphael/*',
      'assets/plugins/flot-chart/*',
      'cdn.datatables.net/*',
    ]
  ];

    public $depends = [
        'yii\bootstrap4\BootstrapAsset',
    ];

  public function init()
  {
    if (strpos(\Yii::$app->request->pathInfo, 'admin/promo-action/attach') !== FALSE) {
      $this->css[] = '//cdn.datatables.net/1.10.19/css/jquery.dataTables.css';
      $this->css[] = '//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/css/dataTables.checkboxes.css';
      $this->js[] = '//cdn.datatables.net/1.10.19/js/jquery.dataTables.js';
      $this->js[] = '//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js';
      $this->js[] = 'assets/pages/attach_models.js';
    }

    if (
      \Yii::$app->request->pathInfo == 'admin/' ||
      \Yii::$app->request->pathInfo == 'admin/default' ||
      \Yii::$app->request->pathInfo == 'admin/default/' ||
      \Yii::$app->request->pathInfo == 'admin/default/index/' ||
      \Yii::$app->request->pathInfo == 'admin/default/index' ||

      \Yii::$app->request->pathInfo == 'moderator/' ||
      \Yii::$app->request->pathInfo == 'moderator/default' ||
      \Yii::$app->request->pathInfo == 'moderator/default/' ||
      \Yii::$app->request->pathInfo == 'moderator/default/index/' ||
      \Yii::$app->request->pathInfo == 'moderator/default/index'
    ) {

      $this->js[] = 'assets/plugins/flot-chart/jquery.flot.min.js';
      $this->js[] = 'assets/plugins/flot-chart/jquery.flot.pie.js';
      $this->js[] = 'assets/plugins/flot-chart/jquery.flot.tooltip.min.js';
      $this->js[] = 'assets/pages/flot.init.js';
    }

    parent::init();
  }
}
