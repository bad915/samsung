<?php

namespace app\modules\admin;

use yii\filters\AccessControl;
use yii\filters\AccessRule;
use app\modules\admin\assets\AdminThemeAsset;

/**
 * admin module definition class
 */
class AdminModule extends \yii\base\Module
{

  /**
   * {@inheritdoc}
   */
  public $controllerNamespace = 'app\modules\admin\controllers';

  public function init()
  {
    parent::init();
    // initialize the module with the configuration loaded from config.php
    \Yii::configure($this, require __DIR__ . '/config.php');

    $this->layout = 'main';
    \Yii::$app->view->title = \Yii::t('app', 'Administration');
    AdminThemeAsset::register(\Yii::$app->view);
  }

  public function getViewPath()
  {
    return \Yii::getAlias('@app/modules/admin/views');
  }

  public function getLayoutPath()
  {
    return \Yii::getAlias('@app/modules/admin/views/layouts');
  }


  /*
* Controller behaviours
*/
  public function behaviors() {
    $behaviors = parent::behaviors();

    $behaviors['access'] = [
      'class' => AccessControl::class,
      'ruleConfig' => [
        'class' => AccessRule::class,
      ],
      'rules' => [
        [
          'allow' => true,
          'roles' => ['admin'],
        ],
      ],
    ];

    return $behaviors;
  }

}
