<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\PromoAction;
use app\models\Variables;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword(Yii::$app->request->post()['User']['password']);
            $model->setAuthKey();
            $model->setAccessToken(Yii::$app->request->post()['User']['password']);

            if ($model->save()) {
                $model->setRole(Yii::$app->request->post()['User']['role']);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

//        $model->load(Yii::$app->request->post());
//        $model->validate();
//        print_r($model->errors);exit;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
          if (Yii::$app->request->post()['User']['password'] != '') {
            $model->setPassword(Yii::$app->request->post()['User']['password']);
            $model->setAuthKey();
            $model->setAccessToken(Yii::$app->request->post()['User']['password']);
          }

          if (!empty(Yii::$app->request->post()['User']['role'])) {
            $model->setRole(Yii::$app->request->post()['User']['role']);
          }

          if (!empty(\Yii::$app->authManager->getRolesByUser($id)['moderator'])) {
            if (!empty(Yii::$app->request->post()['permitted_categories'])) {
              Variables::set('permitted_categories_' . $id, array_keys(Yii::$app->request->post()['permitted_categories']));
            }
            else {
              Variables::set('permitted_categories_' . $id, []);
            }

            if (!empty(Yii::$app->request->post()['permitted_promos'])) {
              Variables::set('permitted_promos_' . $id, array_keys(Yii::$app->request->post()['permitted_promos']));
            }
            else {
              Variables::set('permitted_promos_' . $id, []);
            }
          }

          $model->save();
          return $this->redirect(['/admin/user/index']);
        }

        if (!empty(\Yii::$app->authManager->getRolesByUser($id)['moderator'])) {
          $categories = Category::find()->select(['category_code', 'category_human_name'])->asArray()->all();
          $category_options = [];
          foreach ($categories as $category) {
            $category_options[$category['category_code']] = $category['category_human_name'];
          }

          $promo_actions = PromoAction::find()
            ->andFilterWhere(['>', 'promo_action.end_date', date('Y-m-d H:i:t')])
            ->select(['code_name', 'name'])
            ->asArray()
            ->all();
          $promo_action_options = [];
          foreach ($promo_actions as $action) {
            $promo_action_options[$action['code_name']] = $action['name'];
          }

          return $this->render('update-moderator', [
            'model' => $model,
            'categories' => $category_options,
            'promo_action' => $promo_action_options,
            'permitted_categories' => (array) Variables::get('permitted_categories_' . $id),
            'permitted_promos' => (array) Variables::get('permitted_promos_' . $id)
          ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
