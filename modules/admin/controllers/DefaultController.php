<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use app\models\PublishedActionSearch;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new PublishedActionSearch();
        $params = \Yii::$app->request->queryParams;

        $dataProvider = $searchModel->search($params);
        $query = $dataProvider->query;
        $query->select('COUNT(`published_action`.`category`) as category_count, (SELECT `category_human_name` FROM `category` WHERE `category`.`category_code`=`published_action`.`category`) as category_human_name');
        $query->groupBy('published_action.category');
        
        $promosList = \app\models\PublishedAction::find()
                ->select(['promo_action_code'])
                ->groupBy('promo_action_code')
                ->column();

        if (
          \Yii::$app->request->get('date_start') != '' &&
          \Yii::$app->request->get('date_end') != ''
        ) {
            $date_start = \Yii::$app->request->get('date_start') . ' 00:00:00';
            $date_end = \Yii::$app->request->get('date_end') . ' 23:59:59';
            $query->andFilterWhere(['between', 'created_at', strtotime($date_start), strtotime($date_end)]);
        }

        $data = $query->asArray()->all();

        $datas = [];
        $pielabels = [];
        foreach ($data as $line) {
            $datas[] = (int) $line['category_count'];
            $pielabels[] = $line['category_human_name'];
        }

        return $this->render('index', [
          'data' => $data,
          'datas' => $datas,
          'pielabels' => $pielabels,
          'promosList' => array_combine($promosList, $promosList)
        ]);
    }
}
