<?php

namespace app\modules\admin\controllers;
use \app\modules\admin\models\WorkTimeRangeForm;

class SettingsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new WorkTimeRangeForm();
        $timeSettingValue = NUll;
        $errors = NULL;

        $timeSetting = \app\models\Variables::find()->where(['name' => 'timeFrame'])->one();
        $post = \Yii::$app->request->post();

        if (!empty($post) && !empty($post['WorkTimeRangeForm'])) {
          $model->load($post);
          $model->validate();
          $errors = $model->getErrors();

          if (empty($errors)) {
            if (empty($timeSetting)) {
              $timeSetting = new \app\models\Variables();
            }

            $timeSetting->name = 'timeFrame';
            $timeSetting->value = json_encode($post['WorkTimeRangeForm']);
            $timeSetting->save();
          }
        }

        if (empty($errors) && !empty($timeSetting)) {
          $timeSettingValue = json_decode($timeSetting->value, TRUE);

          foreach ($model->attributes() as $attribute) {
            $model->{$attribute} = !empty($timeSettingValue[$attribute]) ? $timeSettingValue[$attribute] : NULL;
          }
        }

        return $this->render('index', [
            'timeFrame' => $timeSettingValue,
            'model' => $model,
        ]);
    }

}
