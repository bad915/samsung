<?php

namespace app\modules\admin\controllers;

use app\models\ProductModel;
use Yii;
use app\models\PromoAction;
use app\models\PromoActionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PromoActionController implements the CRUD actions for PromoAction model.
 */
class PromoActionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                    'attach' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all PromoAction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PromoActionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PromoAction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PromoAction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PromoAction();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['attach', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PromoAction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['attach', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

  /**
   * Attaches models to an existing PromoAction model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionAttach($id)
  {
    $model = $this->findModel($id);
    $productModels = ProductModel::find()
      ->leftJoin('category', 'category.id = model.category_id')
      ->select(['category.category_code as category', 'model.model', 'model.id'])
      ->orderBy('id')
      ->asArray()
      ->all();

    $alreadyAssigned = \app\models\ModelAssign::find()
      ->select(['model_id'])
      ->orderBy('model_id')
      ->where([
        'entity_type' => 'promo_action',
        'entity_id' => $id,
      ])
      ->column();

    $models = Yii::$app->request->post('models');
    if (!empty($models) && $model->setModels($models)) {
      return $this->redirect(['view', 'id' => $model->id]);
    }

    return $this->render('attach', [
      'model' => $model,
      'modelOptions' => $productModels,
      'alreadyAssigned' => array_combine($alreadyAssigned, $alreadyAssigned)
    ]);
  }

    /**
     * Deletes an existing PromoAction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PromoAction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PromoAction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PromoAction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
