<?php

namespace app\modules\admin\controllers;

use app\models\User;
use Yii;
use app\models\PublishedAction;
use app\models\PublishedActionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * PublishedActionController implements the CRUD actions for PublishedAction model.
 */
class PublishedActionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST', 'GET'],
                    'index' => ['POST', 'GET'],
                ],
            ],
        ];
    }

    /**
     * Lists all PublishedAction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PublishedActionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, null, 25);
        $post = \Yii::$app->request->post();

        $query = \app\models\PromoAction::find()
                ->select(['name', 'code_name']);
        $query->andFilterWhere(['<', 'start_date', date('Y-m-d H:i:t')]);
        $query->andFilterWhere(['>', 'end_date', date('Y-m-d H:i:t')]);
        $promoActions = $query->asArray()->all();
        
        if (!empty($post) && $post['type'] == 'excel') {
          self::exportXml($dataProvider->query);
        }
        
        if (!empty($post) && $post['type'] == 'photo') {
          self::exportPhotos($dataProvider->query);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'promo_actions' => $promoActions
        ]);
    }

    public static function exportXml($query)
    {
      $spreadsheet = new Spreadsheet();
      $sheet = $spreadsheet->getActiveSheet();
      $data = $query->all();

      $columns = [
        'id' => 'ID',
        'created_at' => 'DATE',
        'category' => 'SHORT',
        'model' => 'MODEL',
        'serial_number' => 'SN_PROMO',
        'full_name' => 'FIO',
        'phone_number' => 'TEL',
        'promo_action_code' => 'PROMO',
      ];

      array_unshift($data, ((object) $columns));

      $rowIterator = $sheet->getRowIterator();
      foreach ($data as $rowData) {
        $rowIndex = $rowIterator->current()->getRowIndex();

        $columnIterator = $sheet->getColumnIterator();
        foreach ($columns as $column => $columnLabel) {
          $columnIndex = $columnIterator->current()->getColumnIndex();

          if ($rowIndex != 1) {
            if ($column == 'created_at') {
              $sheet->setCellValue($columnIndex . $rowIndex, date('Y-m-d H:i:s', (int) $rowData->{$column}));
            }
            else {
              $sheet->setCellValue($columnIndex . $rowIndex, $rowData->{$column});
            }
          }
          else {
            $sheet->setCellValue($columnIndex . $rowIndex, $rowData->{$column});
          }

          $columnIterator->next();
        }

        $rowIterator->next();
      }

      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="export.xlsx"');
      header('Cache-Control: max-age=0');

      $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
      $writer->save('php://output');
      exit;
    }

    public static function exportPhotos($query)
    {
      //$query->select(['id', 'full_name', 'image_1', 'image_2', 'image_3']);
//      $query->orWhere(['not', ['image_1' => 'null']]);
//      $query->orWhere(['not', ['image_2' => 'null']]);
//      $query->orWhere(['not', ['image_2' => 'null']]);
      $data = $query->all();
      
      $zip = new \ZipArchive;
      $zipname = Yii::getAlias('@webroot') . '/uploads/photos-' . date('Y-m-d-H-i-s') . '.zip';
      $zip->open($zipname, \ZipArchive::CREATE);

      if (!empty($data)) {
          foreach ($data as $line) {
              $dirname = $line['full_name'] . ' ' . $line['id'];
              $zip->addEmptyDir($dirname);
              foreach (['image_1', 'image_2', 'image_3'] as $imageFieldName) {
                  $filePath = Yii::getAlias('@webroot') . '/' . $line[$imageFieldName];
                  if (file_exists($filePath) && is_file($filePath)) {
                      $zip->addFile($filePath, $dirname . '/' . basename($line[$imageFieldName])); 
                  }
              }
          }
      }

      $zip->close();
      
        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=' . basename($zipname));
        header('Content-Length: ' . filesize($zipname));
        readfile($zipname);
        exit;
    }

    /**
     * Displays a single PublishedAction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $post = \Yii::$app->request->post();

        if (!empty($post) && $post['type'] == 'photo') {
          self::exportPhotos(PublishedAction::find()->where(['id' => $id]));
        }
        
        if (!empty($post) && $post['type'] == 'excel') {
          self::exportXml(PublishedAction::find()->where(['id' => $id]));
        }

        $comments = \app\models\Comment::find()->where([
          'entity_id' => $id,
          'entity_type' => 'published_action'
        ])
          ->asArray()
          ->all();

        foreach ($comments as &$comment) {
          $user = User::find()->where(['id' => $comment['user_id']])->one();
          if (!empty($user)) {
            $comment['user'] = $user->username;
          }
          else {
            $comment['user'] = $comment['user_id'];
          }
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'comments' => $comments
        ]);
    }

    /**
     * Creates a new PublishedAction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PublishedAction();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PublishedAction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PublishedAction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PublishedAction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PublishedAction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PublishedAction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
