<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<h1><?=Yii::t('app', 'Update')?>: <?=$model->username?></h1>

<div class="user-form">

  <?php $form = ActiveForm::begin(); ?>

  <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

  <?php print $form->field($model, 'email')->input('email') ?>

  <?= $form->field($model, 'user_status')->dropDownList([
    1 => \Yii::t('app', 'Active'),
    0 => \Yii::t('app', 'Inactive')
  ]) ?>

  <!--    <?php //print $form->field($model, 'city')->textInput(['maxlength' => true]) ?>-->

  <!--    --><?php //print $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'password')->passwordInput(['minlength' => 6]) ?>

  <div class="row">
    <?php if (!empty($categories)): ?>
      <div class="col-sm-6">
        <h3><?=Yii::t('app', 'Permitted categories')?></h3>
        <?php foreach ($categories as $key => $value): ?>
          <div class="form-group form-check">
            <label class="form-check-label">
              <?= Html::checkbox("permitted_categories[{$key}]", in_array($key, $permitted_categories), ['class' => 'form-check-input']) ?>
              <?=$value?>
            </label>
          </div>
        <?php endforeach;?>
      </div>
    <?php endif; ?>

    <?php if (!empty($promo_action)): ?>
      <div class="col-sm-6">
        <h3><?=Yii::t('app', 'Permitted promos')?></h3>
        <?php foreach ($promo_action as $key => $value): ?>
          <div class="form-group form-check">
            <label class="form-check-label">
              <?= Html::checkbox("permitted_promos[{$key}]", in_array($key, $permitted_promos), ['class' => 'form-check-input']) ?>
              <?=$value?>
            </label>
          </div>
        <?php endforeach;?>
      </div>
    <?php endif; ?>
  </div>


  <div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
  </div>

  <?php ActiveForm::end(); ?>

</div>
