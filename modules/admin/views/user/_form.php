<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?php print $form->field($model, 'email')->input('email') ?>

    <?php print $form->field($model, 'role')->dropDownList([
        'promoter' => 'Promoter',
        'moderator' => 'Moderator',
    ]) ?>

  <?= $form->field($model, 'user_status')->dropDownList([
    1 => \Yii::t('app', 'Active'),
    0 => \Yii::t('app', 'Inactive')
  ]) ?>

<!--    <?php //print $form->field($model, 'city')->textInput(['maxlength' => true]) ?>-->

<!--    --><?php //print $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['minlength' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
