<?php
use yii\jui\DatePicker;
?>
<script>
  window.pieChartDatas = <?=json_encode($datas)?>;
  window.pieChartLabels = <?=json_encode($pielabels)?>;
</script>
<div class="row">
  <div class="col-lg-6">
    <div class="card m-b-30">
      <div class="card-body">

        <h4 class="mt-0 header-title"><?=\Yii::t('app','Action activity')?></h4>

        <form style="margin-bottom: 20px;" action="" method="get" class="form-inline" role="form">
          <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                 value="<?=Yii::$app->request->csrfToken?>"/>

          <div class="form-group"> 
          <?php
          print yii\helpers\Html::dropDownList('PublishedActionSearch[promo_action_code]', (!empty($_GET['PublishedActionSearch']['promo_action_code']) ? $_GET['PublishedActionSearch']['promo_action_code'] : null), array_merge([null => Yii::t('app', 'All')], $promosList), ['class' => 'form-control']);
          ?>
          </div>
          &nbsp;
          <div class="form-group">
            <?php
            echo DatePicker::widget([
              'name' => 'date_start',
              'language' => 'en',
              'value' => Yii::$app->request->get('date_start'),
              'dateFormat' => 'yyyy-MM-dd',
              'options' => [
                'class' => 'form-control',
                'placeholder' => 'Start date'
              ]
            ]);
            ?>
          </div>
          &nbsp;
          <div class="form-group">
            <div class="form-group">
              <?php
              echo DatePicker::widget([
                'name' => 'date_end',
                'language' => 'en',
                'value' => Yii::$app->request->get('date_end'),
                'dateFormat' => 'yyyy-MM-dd',
                'options' => [
                  'class' => 'form-control',
                  'placeholder' => 'End date' ]
              ]);
              ?>
            </div>
          </div>
          &nbsp;

          <button class="btn btn-info" type="submit">
            Apply
          </button>
        </form>

        <div id="pie-chart">
          <div id="pie-chart-container" class="flot-chart" style="height: 320px">
          </div>
        </div>

      </div>
    </div>
  </div> <!-- end col -->
</div>