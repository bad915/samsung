<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Models';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Model', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'category_id' => [
              'label' => 'Category',
              'attribute' => 'category_id',
              'content' => function($model) {
                return $model->category;
              },
              'filter' => \app\models\Category::find()->indexBy('id')->select('category_human_name')->column()
            ],
            'model',
            ['class' => '\app\modules\admin\models\ActionColumn'],
        ],
        'pager' => [
          'linkOptions' => ['class' => 'page-link'],
          'disabledPageCssClass' => 'page-link disabled'
        ],
    ]); ?>
</div>
