<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductModel */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
  window.modelmaskKeyUp = function (elem) {
    $(elem).val($(elem).val().replace(/[^a-zA-Z0-9]/i, ""));
  }
</script>

<div class="model-form">
  <?php if (!empty($categories)): ?>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList($categories) ?>

    <?= $form->field($model, 'model')->widget(\yii\widgets\MaskedInput::className(), [
      'mask' => '[9|A|-]{20}',
      'clientOptions' => [
          'placeholder' =>  ''
      ],
//      'options' => [
//        'onKeyUp' => 'window.modelmaskKeyUp(this)',
//        'class' => 'form-control'
//      ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

  <?php else: ?>
    <?= Yii::t('app', 'Please add a category') ?>
  <?php endif; ?>
</div>
