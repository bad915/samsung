<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PromoAction */

$this->title = 'Create Promo Action';
$this->params['breadcrumbs'][] = ['label' => 'Promo Actions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
