<script>
  window.databaleData =  <?=json_encode($modelOptions)?>;
  window.alreadyAssigned =  <?=json_encode($alreadyAssigned)?>;
</script>

<form id="attach-models" method="post">

  <table id="table">
    <thead>

    <th>
    </th>
    <th>Model</th>
    <th>Category</th>

    </thead>
  </table>

  <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

  <button class="btn btn-success" type="submit"><?=Yii::t('app', 'Attach models')?> <i class="ion-plus"></i> </button>
</form>

