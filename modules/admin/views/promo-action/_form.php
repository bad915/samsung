<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PromoAction */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
  window.code_namemaskKeyUp = function (elem) {
    $(elem).val($(elem).val().replace(/[^a-zA-Z0-9]/i, ""));
  }
</script>

<div class="promo-action-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'code_name')->widget(\yii\widgets\MaskedInput::className(), [
      'mask' => '[9|A]{30}',
      'clientOptions' => [
        'placeholder' =>  ''
      ],
      'options' => [
        //'onKeyUp' => 'window.code_namemaskKeyUp(this)',
        'class' => 'form-control'
      ]
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList([
        1 => \Yii::t('app', 'Active'),
        0 => \Yii::t('app', 'Inactive')
    ]) ?>

    <div class="form-group field-start-date required">
      <label class="control-label" for="start_date">Start date</label>
      <?php
      echo \janisto\timepicker\TimePicker::widget([
        'language' => 'ru',
        'model' => $model,
        'attribute' => 'start_date',
        'mode' => 'datetime',
        'addon' => '<i class="ion-calendar"></i>',
        'clientOptions'=>[
          'dateFormat' => 'yy-mm-dd',
          'timeFormat' => 'HH:mm:ss',
          'showSecond' => true,
        ]
      ]);
      ?>
      <div class="help-block"></div>
    </div>

    <div class="form-group field-end-date required">
      <label class="control-label" for="end_date">End date</label>
      <?php
      echo \janisto\timepicker\TimePicker::widget([
        'language' => 'ru',
        'model' => $model,
        'attribute' => 'end_date',
        'mode' => 'datetime',
        'addon' => '<i class="ion-calendar"></i>',
        'clientOptions'=>[
          'dateFormat' => 'yy-mm-dd',
          'timeFormat' => 'HH:mm:ss',
          'showSecond' => true,
        ]
      ]);
      ?>
      <div class="help-block"></div>
    </div>


  <div class="form-group">
    <?= Html::submitButton(Yii::t('app', 'Save and attach models'), ['class' => 'btn btn-success']) ?>
  </div>

    <?php ActiveForm::end(); ?>

</div>
