<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PromoActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Promo Actions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-action-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Promo Action', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'code_name',
            'status',
            'start_date',
            'end_date',
            //'location',
            //'city',
            //'created_at',
            //'updated_at',

            [
                'class' => '\app\modules\admin\models\ActionColumn',
                'template' => '{view} {update} {delete} {attach}',
                'buttons'=>[
                  'attach'=>function ($url, $model) {
                    $t = '/admin/promo-action/attach?id='.$model->id;
                    return '<a href="' . $t . '"><i class="ion-plus"></i></a>';
                  },
                ],
            ],
        ],
    ]); ?>
</div>
