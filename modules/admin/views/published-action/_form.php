<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PublishedAction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="published-action-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'promo_action_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

  <div class="form-group field-start-date required">
    <label class="control-label" for="start_date"><?=Yii::t('app', 'Date')?></label>
    <?php
    echo \janisto\timepicker\TimePicker::widget([
      'language' => 'ru',
      'model' => $model,
      'attribute' => 'date',
      'mode' => 'datetime',
      'addon' => '<i class="ion-calendar"></i>',
      'clientOptions'=>[
        'dateFormat' => 'yy-mm-dd',
        'timeFormat' => 'HH:mm:ss',
        'showSecond' => true,
      ]
    ]);
    ?>
    <div class="help-block"></div>
  </div>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'serial_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'sms_code')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
