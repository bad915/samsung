<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PublishedAction */

$this->title = Yii::t('app', 'Create Published Action');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Published Actions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="published-action-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
