<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Settings';
?>
<div class="form-group">
  <div class="col-sm-4">
    <div class="card m-b-30">
      <div class="card-body">
        <h4 class="mt-0 header-title"><?=Yii::t('app', 'System api active time frame')?></h4>
        <?php $form = ActiveForm::begin([
          'id' => 'login-form',
          'layout' => 'horizontal',
          'fieldConfig' => [
            'template' => "{label}\n<div>{input}</div>\n<div>{error}</div>",
            'labelOptions' => ['class' => 'control-label'],
          ],
        ]); ?>

        <?= $form->field($model, 'start_time')->widget(\yii\widgets\MaskedInput::className(), [
          'mask' => '99:99',
          'clientOptions' => [
            'placeholder' =>  'hh:mm'
          ],
        ]) ?>

        <?= $form->field($model, 'end_time')->widget(\yii\widgets\MaskedInput::className(), [
          'mask' => '99:99',
          'clientOptions' => [
            'placeholder' =>  'hh:mm'
          ],
        ]) ?>

            <?= $form->field($model, 'status')->checkbox([
              'template' => "<div>{input} {label}</div>\n<div>{error}</div>",
            ]) ?>

        <div>
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>

          <p class="small" style="padding-top: 10px">
            Current date : <?=date('Y-m-d H:i');?>
          </p>
        </div>

        <?php ActiveForm::end(); ?>

      </div>
    </div>
  </div>
</div>
