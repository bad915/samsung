<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
  window.category_codemaskKeyUp = function (elem) {
      $(elem).val($(elem).val().replace(/[^a-zA-Z0-9]/i, ""));
  }
</script>

<div class="category-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'category_code')->widget(\yii\widgets\MaskedInput::className(), [
      'mask' => '[9|A]{5}',
      'clientOptions' => [
          'placeholder' =>  ''
      ],
//      'options' => [
//        'onKeyUp' => 'window.category_codemaskKeyUp(this)',
//        'class' => 'form-control'
//      ]
    ]) ?>

    <?= $form->field($model, 'category_human_name')->textInput(['maxlength' => true]) ?>

  <?php
    if (!empty($model->image)) {
        print Yii::$app->thumbnail->img(\Yii::getAlias('@webroot') . '/' . $model->image, [
          'thumbnail' => [
            'width' => 400,
            'height' => 400
          ]
        ]);;
    }
  ?>

    <?= $form->field($model, 'upload')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
