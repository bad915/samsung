<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="source-message-form">

    <?php $form = ActiveForm::begin(); ?>

  <div class="row">
    <div class="col-sm-6"><?= $form->field($model, 'message')->textInput(['required' => TRUE, 'disabled' => 'disabled']) ?></div>
    <div class="col-sm-6"><?= $form->field($model, 'translation')->textInput(['required' => TRUE]) ?></div>
  </div>


    <div class="form-group">
        <?= Html::submitButton('Сохраниить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
