<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class WorkTimeRangeForm extends Model
{
  public $start_time;
  public $end_time;
  public $status;

  /**
   * @return array the validation rules.
   */
  public function rules()
  {
    return [
      // username and password are both required
      [['start_time', 'end_time'], 'required'],
      [['start_time', 'end_time'], 'validateTime'],
    ];
  }

  public function validateTime($attribute)
  {
    if (preg_match('/\d{2}:\d{2}/', $this->{$attribute})) {
      list($hour, $minute) = explode(':', $this->{$attribute});
      if (
        $hour > 23 ||
        $minute > 59
      ) {
        $this->addError($attribute, 'Incorrect datetime');
      }
    }
    else {
      $this->addError($attribute, 'Incorrect datetime');
    }
  }
}
